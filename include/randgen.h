/*
Used to create random unsigned long using different methods
For now, only one generator has been implemented.
*/

#include <stdio.h>
#include <stdlib.h>

//Generates a pseudorandom unsigned long based on the file /dev/urandom which can be found in Unix based operating systems 
unsigned long randgen_urandom();