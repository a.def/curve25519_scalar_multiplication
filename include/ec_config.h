/*
File storing constants which are dependent on the curve type, encryption strength, etc... But also test variables
*/
#ifndef __EC_CONFIG_H
#define __EC_CONFIG_H

//Number of bits on which rational point coordinates are stored.
#define N_BITS 256
#define KEY_SIZE 256

//Parameters of elliptic curve equation
#define A 486662LL
#define B 7
 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TEST MACROS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Macros used for some simple tests
#define PRIME 98689 //31013, 30803, 86743, 98689, 149
#define MOD PRIME


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DO NOT MODIFY THE CODE BELLOW~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define TEMP_LEN 6  //Length of the tempoary table used to make mpz calculations. It should be set to 6. Lowering it would yield wrong results. Increasing it's usefull


#endif
