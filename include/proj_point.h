#ifndef __PROJ_POINT_H
#define __PROJ_POINT_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "gmp_config.h"

typedef struct {
    mpz_t X;
    mpz_t Y;
    mpz_t Z;
}   _projective_coordinates_struct;

typedef _projective_coordinates_struct* proj_point;


#ifndef ARDUINO_ARCH_AVR
    /*Prints the coordinates of point

    Name allows for custom naming of the point at printing

    prints: [name] = (X : Y : Z)\n
    */
    void proj_point_printf(proj_point point, char name[]);
#endif

//proj_point creators
proj_point proj_point_init(); 

//free used memory
void proj_point_free(proj_point target);

//copies the coordinates of source to target
void proj_point_set(proj_point target, proj_point source);

/*Sets the coordinates of the target

The coordinates can be changed in several manners, depending on the arguments:
    - X, Y, Z can be null pointers. In which case, the corresponding argument will not be changed
*/
void proj_point_set_mpz(proj_point target, mpz_t X, mpz_t Y, mpz_t Z);

/*proj_point setter*/
void proj_point_set_ui(proj_point target, unsigned long X, unsigned long Y, unsigned long Z);

/*Setter function of proj_point using strings in a given base

-Ttring arguments can be NULL if they are not to be modified
-The base argument is the base in which the string arguments are written
*/
void proj_point_set_str(proj_point target, const char* str_X, const char* str_Y, const char* str_Z, int base);


/*result = source mod order*/
void proj_point_mod(proj_point result, proj_point source, mpz_t order) ;

/*
Converts source (X : Y : Z) to (X/Z : Y/Z : 1).

Can be used before proj_pointString to print (x, y), as x = X/Z and y = Y/Z
*/
void proj_point_normalize(proj_point result, proj_point source, mpz_t order);

/*This function converts point to a character array of digits in the given base

-If str is NULL, the function will allocate the necessary block storage. Good for single use cases such as printf.

-If str is not NULL, be sure to have a block of storage large enough to store. Minimum block size is:
------>mpz_sizeinbase(point->X, base) + mpz_sizeinbase(point->Y, base) + mpz_sizeinbase(point->Y, base) + 7

A pointer to the result string is returned, being either the allocated block or the given str.
*/
char* proj_point_toString_raw(char str[], int base, proj_point point);

char* proj_point_toString(char str[], int base, proj_point point);

char* proj_point_toString_debug(int base, proj_point point) ;



#endif