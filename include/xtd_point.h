#ifndef __XTD_POINT_H
#define __XTD_POINT_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "gmp_config.h"

typedef struct {
    mpz_t X;
    mpz_t Y;
    mpz_t T;
    mpz_t Z;
}   _extended_coordinates_struct;

typedef _extended_coordinates_struct* xtd_point;

#ifndef ARDUINO_ARCH_AVR
    /*Prints the coordinates of point

    Name allows for custom naming of the point at printing

    prints: [name] = (X : Y : T : Z)\n
    */
    void xtd_point_printf(xtd_point point, char name[]);
#endif

//xtd_point creators
xtd_point xtd_point_init(); 

//free used memory
void xtd_point_free(xtd_point target);

//copies the coordinates of source to target
void xtd_point_set(xtd_point target, xtd_point source);

/*Sets the coordinates of the target

The coordinates can be changed in several manners, depending on the arguments:
    - X, Y, T, Z can be null pointers. In which case, the corresponding argument will not be changed
*/
void xtd_point_set_mpz(xtd_point target, mpz_t X, mpz_t Y, mpz_t T, mpz_t Z);

/*Setter function of xtd_point using strings in a given base

-Ttring arguments can be NULL if they are not to be modified
-The base argument is the base in which the string arguments are written
*/
void xtd_point_set_str(xtd_point target, const char* str_X, const char* str_Y, const char* str_T, const char* str_Z, int base);

/*xtd_point setter*/
void xtd_point_set_ui(xtd_point target, unsigned long X, unsigned long Y, unsigned long T, unsigned long Z);

/*result = source mod order*/
void xtd_point_mod(xtd_point result, xtd_point source, mpz_t order) ;


/*This function converts point to a character array of digits in the given base

-If str is NULL, the function will allocate the necessary block storage. Good for single use cases such as printf.

-If str is not NULL, be sure to have a block of storage large enough to store. Minimum block size is:
------>mpz_sizeinbase(point->x, base) + mpz_sizeinbase(point->y, base) + 8

A pointer to the result string is returned, being either the allocated block or the given str.
*/
char* xtd_point_toString(char str[], int base, xtd_point point);



#endif