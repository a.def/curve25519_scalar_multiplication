
#ifndef __ECLIB_H
#define __ECLIB_H

#include <assert.h>

#include "gmp_extension.h"
#include "ec_config.h"
#include "ec_point.h"
#include "proj_point.h"
#include "xtd_point.h"

//#if __ARDUINO_MODE == 0 //If not in arduino mode, then it uses normal gmp library, so these function can be used
#ifndef ARDUINO_ARCH_AVR
    /*This functions tries to generate a random point on the ec.

    Returns:
        - 1 if the point generated is indeed on the ec.
        - 0 if the point is not on the ec.

    ec_point target:
        - pointer which stores the x and y coordinates of the generated point, modulo order.
    */
    int random_ec_point(ec_point target,  mpz_t temp[], gmp_randstate_t state, mpz_t order) ;

    /*Generates a random point on the EC

    ec_point target:
        - pointer which stores the x and y coordinates of the generated point, modulo order.

    returns the number of tries it took to find the point
    */
    size_t ec_point_generator(ec_point target, mpz_t temp[], gmp_randstate_t state, mpz_t order);
#endif



/*
 +-+-+-+-+-+-+ +-+-+-+-+-+
 |S|I|M|P|L|E| |C|U|R|V|E|
 +-+-+-+-+-+-+ +-+-+-+-+-+
*/

/*This function calculates the value of x^3 + A*x + B and stores it in y
The value in y is modulo order*/
void ec_simple_equation_calculation(mpz_t y, mpz_t x, mpz_t temp[], mpz_t order); 

/*Elliptic curve standard doubling ECD

Calculates result = ECD(point1, point2) 

Returns an int which can be:
> INFINITY if the result is at infinity
> NOT_INFINITY if the result is not at infinity

BEWARE: This function does NOT verify whether point1 and point2 are actually on the curve.*/
int ec_point_doubling(ec_point result, ec_point p, mpz_t temp[], mpz_t order); //FIXME probleme au niveau des y qui ne bougent pas...

/*Elliptic curve standard addition ECA

Calculates result = ECA(point1, point2) 

Returns an int which can be:
> INFINITY if the result is at infinity
> NOT_INFINITY if the result is not at infinity
> (-1) if there is an unknown case

BEWARE: This function does NOT verify whether point1 and point2 are actually on the curve.*/
int ec_point_addition(ec_point res, ec_point point1, ec_point point2, mpz_t temp[],  mpz_t order); //returns 1 if the addition result is at infinity

/*This function allows for a quick calculation of scalar multiples of an ec point.

result and source can point to a same point in memory. The coords of source will be replaced to the new ones.*/
void ec_montgomery_ladder(ec_point result, ec_point source, mpz_t scalar, mpz_t temp[], mpz_t order);



/*
+-+-+-+-+-+-+-+
 |E|D|W|A|R|D|S|
 +-+-+-+-+-+-+-+
*/

/*This function calculates the addition on point1 with point2 and outputs the result into res

Beware: the output is NOT modulo order

-X3 = (X1*Y2 + Y1*X2) * (Z1*Z2 - d*T1*T2)
-Y3 = (Z1*Z2 + d*T1*T2) * (Z1*Z2 + d*T1*T2)
-T3 = (X1*Y2 + Y1*X2) * (Y1Y2 - a*X1*X2)
-Z3 = (Z1*Z2 - d*T1*T2) * (Z1*Z2 + d*T1*T2)
*/
void edwards_addition(xtd_point res, xtd_point point1, xtd_point point2, mpz_t temp[], mpz_t a, mpz_t d);

/*
This function calculate the scalar multiplicaiton of a point on an edwards curve using unified addition law
NOT SECURE

result = [scalar]source modulo [order]

result and source can point to the same place in memory.
*/
void edwards_scalar_multiplication(xtd_point result, xtd_point source, mpz_t scalar, mpz_t temp[], mpz_t order, mpz_t a, mpz_t d);
/*
+-+-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+
 |M|O|N|T|G|O|M|E|R|Y| |C|U|R|V|E|
 +-+-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+
*/
#ifndef ARDUINO_ARCH_AVR
    int random_ed_point(proj_point target,  mpz_t temp[], gmp_randstate_t state, mpz_t order) ; //TODO; fautil vraiment utiliser un nombre random?
    size_t proj_point_generator(proj_point target, mpz_t temp[], gmp_randstate_t state, mpz_t order);  //TODO; fautil vraiment utiliser un nombre random?
#endif
/*
This function tries to calculate the y value of a given x coordinate.

Return 1 if the point exist on the curve, else 0
*/
int curve25519_calculation(mpz_t y, mpz_t x, mpz_t temp[], mpz_t order);

void okeya_sakurai_y_recovery(proj_point result, proj_point p, proj_point q, proj_point plus, mpz_t temp[],  mpz_t order);

/* 
This function realises the differential addition of p and q.

Function parameters:

- result : result of the addition. Can point to the same proj_point as p or q
- p, q : input points of the addition
- diff : difference between p and q. In montgomery ladder, diff is the initial input point of the ladder
- temp[] : calculation stack to store intermediary mpz values
- order : order of the finite field over which the curve is defined
*/
void montgomery_differential_addition(proj_point result, proj_point p, proj_point q, proj_point diff, mpz_t temp[], mpz_t order);

/*
This function realises the differential doubling of p.

Function parameters:

- result : result of the addition. Can point to the same proj_point as p or q
- p, q : input points of the addition
- diff : difference between p and q. In montgomery ladder, diff is the initial input point of the ladder
- temp[] : calculation stack to store intermediary mpz values
- order : order of the finite field over which the curve is defined
*/
void montgomery_differential_doubling(proj_point result, proj_point p, mpz_t temp[], mpz_t order);


/*
TODO: NOT IMPLEMENTED YET -- This function calculates the doubling and addition required for montgomery ladder.

Function parameters

- add_res: result of the addition of p and q
- dbl_res: result of the doubling of dbl
- dbl : input point that needs to be doubled
- p : first point of the differential addition
- q : second point of the differential addition
- diff : p-q, required to successfully do differential addition. In the montgomery ladder algorithm, this is equal to the initial value of p.
- temp[] : a calculation stack to store intermediary mpz values
- order : order of the finite field

Differential addition and doubling are put into function in order to reduce the number of addition functions by 2
*/
void montgomery_differential_addition_doubling(proj_point add_res, proj_point dbl_res, proj_point dbl, proj_point p, proj_point q, proj_point diff, mpz_t temp[], mpz_t order);

/*
This function calculate the scalar multiplicaiton of a point on a montgomery ladder using the montgomery ladder algorithm

result = [scalar]source modulo [order]

result and source can point to the same place in memory.

FOR YOUR INFORMATION: The result is a non normalized proj_point, aka Z =! 0.
*/
void montgomery_differential_ladder(proj_point result, proj_point source, mpz_t scalar, mpz_t temp[], mpz_t order);


void montgomery_differential_ladder_str(proj_point result, proj_point source, mpz_t scalar, mpz_t temp[], mpz_t order);

void montgomery_differential_y_recovery();

#endif

