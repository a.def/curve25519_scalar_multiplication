#ifndef __GMP_CONFIG_H
#define __GMP_CONFIG_H

#ifdef ARDUINO_ARCH_AVR
    // #include <gmp-ino.h>
    #include <gmp-ino.h>
#else
     #define ARDUINO_ARCH_AVR
     #include "mini-gmp.h"
    //#include <gmp.h>
#endif

// #define __ARDUINO_MODE 1
// //0 ; library used with gmp, can use random state
// //1 ; library used for arduino




// #if __ARDUINO_MODE == 1
//     #include <gmp-ino.h>
// #else
//     #include <gmp.h>
// #endif

#endif