#ifndef __EC_POINT_H
#define __EC_POINT_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "gmp_config.h"

#define EC_INFINITY 1
#define EC_NOT_INFINITY 0
 
typedef struct {
    mpz_t x;
    mpz_t y;
    int infinity; 
} _ec_point_struct;



typedef _ec_point_struct* ec_point;


#ifndef ARDUINO_ARCH_AVR
    /*Prints the coordinates of point

    Name allows for custom naming of the point at printing
    */
    void ec_point_printf(ec_point point, char name[]);
#endif

//ec_point creation/initialization
ec_point ec_point_init(); 
ec_point ec_point_init_ui(unsigned x, unsigned y, int infinity);
ec_point ec_point_init_str(char* str_x, char* str_y, int infinity, int base);

//free used memory
void ec_point_free(ec_point target);

//copies the coordinates of source to target
void ec_point_set(ec_point target, ec_point source);

/*Sets the coordinates of the target

The coordinates can be changed in several manners, depending on the arguments:
    - x and y : changes both coordinates of target
    - x and NULL : only changes x coordinate
    - NULL and y : only changes y coordinate

If both x AND y arguments are NULL, then nothing happens to target*/
void ec_point_set_mpz(ec_point target, mpz_t x, mpz_t y, int infinity);

/*Sets the coordinates of the target*/
void ec_point_set_ui(ec_point target, unsigned long x, unsigned long y, int infinity);

/*Sets the coordinates of the target using strings as input

infinity defined as EC_INFINITY or EC_NOT_INFINITY
*/
void ec_point_set_str(ec_point target, char* str_x, char* str_y, int infinity, int base);

/* This functions calculates the coordinates of source modulo order, and stores them in result.

result and source can point to a same point in memory. The coords of source will be replaced to the new ones.
*/
void ec_point_mod(ec_point result, ec_point source, mpz_t order);

/*This function converts point to a character array of digits in the given base

-If str is NULL, the function will allocate the necessary block storage. Good for single use cases such as printf.

-If str is not NULL, be sure to have a block of storage large enough to store. Minimum block size is:
------>mpz_sizeinbase(point->x, base) + mpz_sizeinbase(point->y, base) + 8

A pointer to the result string is returned, being either the allocated block or the given str.
*/
char* ec_point_toString(char str[], int base, ec_point point);

#endif