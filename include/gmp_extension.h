/*
Provides additional mpz_t functions which proved to be convenient, useful or necessary to implement Elliptic Curve Cryptography

These functions include, but are not limited to:
    - Initialising the random generator of the gmp lib
    - Creation, printing and freeing a mpz_t tab
    - New ways to calculate square roots
    - etc...


BEWARE : most of the time, function inputs of type mpz_t are expected to be modulo prime or modulo order !
//FIXME: indicate when the input is required to be moduloed
These functions are built that way to avoid unnecessary mpz_mod calls.
Note that they might still give results, however, they won't perform the way they were intended to work.
*/

#ifndef __GMP_EXTENSION_H
#define __GMP_EXTENSION_H



#include <stdlib.h>
#include "gmp_config.h"
#include "randgen.h"
#include "ec_config.h"

mpz_t* mpz_tab_creation(size_t n);
void mpz_tab_free(mpz_t* tab, size_t n) ;


//#if __ARDUINO_MODE == 0 //only used if it is not running on arduino.
#ifndef ARDUINO_ARCH_AVR
    //initialisation of gmp random state used for gmp random generator functions
    void gmp_random_initialisation(gmp_randstate_t state);
    void mpz_tab_printf(mpz_t tab[], size_t n, char tab_name[]);
    
#endif

/*
This function redefined its standard gmp version, in order to be supported for arduino.

Returns the legendre symbol (y | order). It can be : 
            1 : y is a qudratic res mod order
            0 : y == 0 mod order
            -1 : y non quadratic res 
*/
int mpz_legendre_gmp_extension(mpz_t y, mpz_t tab[], mpz_t order);

int is_quadratic_residue(mpz_t y, mpz_t tab[], mpz_t order);

//Allocates and stores the binary value of op in a string str. Returns the length of str.
size_t mpz_to_binary(char* str, mpz_t op); 

//operations on mpz_t variables
void left_binary_mult(mpz_t op, mpz_t scalar, mpz_t temp[]); //TODO
///left binary exponentiation algorithm. USES THE LAST CELL OF TEMP[] (temp[TEMP_LEN -1])
unsigned long left_binary_exp(mpz_t result, mpz_t source, mpz_t exponent, mpz_t temp[], mpz_t order); //FIXME?? problem with the last bit/

void montgomery_ladder_mult(mpz_t op, mpz_t scalar, mpz_t temp[]);//TODO
void montgomery_ladder_exp(mpz_t n, mpz_t exponent, mpz_t temp[]);//TODO

//Given a prime number prime and a quadratic residue source < prime, this function find a mod-prime root of source and stores it into result. Returns 1 when the result is found.
void tonelli_shanks_algorithm(mpz_t result, mpz_t source, mpz_t temp[], mpz_t prime);



/*Returns in result the root of source in the finite field Fprime

WARNING!! THESE CONDITIONS MUST BE MET :
        - prime must be an odd prime
        - prime must be equal 3 mod 4
        -   source must be a quadratic residue
        

    Then, then prime + 1 is divisible by 4 and the root of source can be calculated using:
        - result = source^{(p + 1) / 4}
*/
void mpz_sqrt_3mod4(mpz_t result, mpz_t source, mpz_t temp[], mpz_t prime);


#endif