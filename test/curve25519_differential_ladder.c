#include "../include/eclib.h"


int main() {
    //variable declaration
    unsigned scalar_input;

    mpz_t scalar;
    mpz_t order;
    mpz_t* temp = mpz_tab_creation(TEMP_LEN);

    proj_point g = proj_point_init();
    proj_point result = proj_point_init();

    //variable initialisation
    printf("Enter a scalar:\n");
    scanf("%u", &scalar_input);
    mpz_init(scalar);
    mpz_init(order);

    mpz_set_str(scalar, "45132874992165642623205429848808932438384574116820264464902714021104563491617", 10); //10209657245381331378292977265087727321333194016471487653012954347636253753048
    mpz_set_str(order, "7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffed", 16);

    proj_point_set_ui(g, 9, 1, 1);
    
    //program
    curve25519_calculation(g -> Y, g -> X, temp, order);
    montgomery_differential_ladder(result, g, scalar, temp, order);
    proj_point_normalize(result, result, order);
    
    //Result printing
    //proj_point_printf(g, "g");
    //proj_point_printf(result, "result");

    char* str_result = proj_point_toString(NULL, 10, result);
    printf("\nYou can verify the result on SageMath (sagecell.sagemath.org) with the following code:\n");
    printf("EC = EllipticCurve(GF(2 ** 255 - 19),[0, 486662, 0, 1, 0])\n");
    printf("G = EC(Integer(9) , Integer(14781619447589544791020593568409986887264606134616475288964881837755586237401))\n");
    printf("G *= %u\n", scalar_input);
    printf("print('public key = ', G)\n");
    printf("print('Is the result correct equal to the sagemath key? ' , (G == EC(%s)))\n", str_result);

    //free memory space
    free(str_result);
    mpz_tab_free(temp, TEMP_LEN);
    mpz_clear(scalar);
    mpz_clear(order);
    proj_point_free(g);
    proj_point_free(result);

}