#include "../include/eclib.h"
#include <stdlib.h>

int main() {
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\nRandom Point Generator for EC: x^3 + %d*x + %d modulo %d\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n", A, B, MOD);


    mpz_t r;
    mpz_init(r);
    mpz_set_ui(r, 2);
    mpz_pow_ui(r, r, 255);
    mpz_sub_ui(r, r, 19);
    size_t len = mpz_sizeinbase(r, 2) ;
    gmp_printf("r = %Zd, number of bits = %lu\n", r, len);

    mpz_t* temp = mpz_tab_creation(TEMP_LEN);


    ec_point p1 = ec_point_init();
    ec_point p2 = ec_point_init();
    ec_point p3 = ec_point_init();


    gmp_randstate_t state;
    gmp_random_initialisation(state);

    printf("\ngenerating p1...\n");

    
    ec_point_generator(p1, temp, state, r);
    ec_point_printf(p1, "p1 modulo prime");

    printf("\ngenerating p2...\n");
    ec_point_generator(p2, temp, state, r);
    ec_point_printf(p2, "p2 modulo prime");

    ec_point_doubling(p3, p2, temp, r);
    ec_point_printf(p3, "2 * p2");


    mpz_clear(r);
    mpz_tab_free(temp, TEMP_LEN);
    gmp_randclear(state);
    ec_point_free(p1);
    ec_point_free(p2);
     ec_point_free(p3);


    return 0;
}
    
