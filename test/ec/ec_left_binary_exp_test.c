#include <stdio.h>
#include "../include/gmp_extension.h"

#define MIN(x, y) (x<y)? return x: return y

void print_bits(mp_bitcnt_t times, unsigned value)
{   
    for (unsigned long int i = 0; i < times; i ++)
    {
        printf("%u", value);
    }
}

void print_bits_number(mpz_t number) {
    mp_bitcnt_t index = 0;
    mpz_setbit(number, index);
    mp_bitcnt_t zero = mpz_scan0(number, index);
    mp_bitcnt_t one = mpz_scan1(number, index);
    while (one != ULONG_MAX)
    {   
        if (one > zero) {
            print_bits(one - zero, 0);
            index = one;
        }
        else {
            print_bits(zero - one, 1);
            index = zero;
        }
        zero = mpz_scan0(number, index);
        one = mpz_scan1(number, index);
    }
}

void print_bits_number2(mpz_t number) {
    mp_bitcnt_t index = 0;
    while (mpz_scan1(number, index) != ULONG_MAX)
    {   
        printf("%u",mpz_tstbit(number, index));
        index++;
    }
    puts("");
}

int main() {

    printf("~~~~~~~~~~~~~~~~~~~~~~~\ntest bit_operation\n~~~~~~~~~~~~~~~~~~~~~~~\n");

    unsigned long int num = 0;
    unsigned long int num2 = 0;
    
    mpz_t exponent;
    mpz_t test;
    mpz_t r;
    mpz_t* temp = NULL;

    mpz_init(test);
    mpz_init(exponent);
    mpz_init(r);
    mpz_set_ui(r, 2);
    mpz_pow_ui(r, r, 255);
    mpz_sub_ui(r, r, 19);
    size_t len = mpz_sizeinbase(r, 2) ;
    gmp_printf("Prime = %Zd, number of bits = %lu\n", r, len);

    temp = mpz_tab_creation(TEMP_LEN);

    printf("Enter the base: ");
    scanf("%lu", &num); 
    mpz_set_ui(test, num);

    printf("Enter the exponent: ");
    scanf("%lu", &num2);
    mpz_set_ui(exponent, num2);

    gmp_printf("%Zd^%Zd = ", test, exponent);
    left_binary_exp(test, test, exponent, temp, r);
    gmp_printf("%Zd\n", test);

    //exponent of _mp_size
    //printf("_mp_size = %d\n", exponent -> _mp_size);
    ////exponent of _mp_alloc
    //printf("_mp_alloc = %d\n", exponent -> _mp_alloc);


    //print_bits_number(exponent);
    //puts("");
    //printf("%lu\n", mpz_sizeinbase(exponent, 2));
    //puts("");

    
    mpz_clear(exponent);
    mpz_clear(test);
    mpz_clear(r);
    mpz_tab_free(temp, TEMP_LEN);
    return 0;
}

