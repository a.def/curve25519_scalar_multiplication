#include "../include/eclib.h"
#include "../include/gmp_extension.h"
#include <gmp.h>
#include <stdio.h>

int main() {
    printf("~~~~~~~~~~~~~~~~~~~~~~~\ntest quadratic_residue\n~~~~~~~~~~~~~~~~~~~~~~~\n");

    mpz_t x;
    mpz_t y;
    mpz_t r;
    mpz_t* temp = mpz_tab_creation(TEMP_LEN);

    unsigned long prime;
    printf("Enter a prime number: ");
    scanf("%lu", &prime);

    unsigned long x_input;
    printf("Enter an x coord: ");
    scanf("%lu", &x_input);

    mpz_init_set_ui(x, x_input);
    mpz_init(y);
    mpz_init_set_ui(r, prime);

    if (is_quadratic_residue(x, temp, r) == 1) {
            printf("quadratic res OK\t");
            //tonelli_shanks_algorithm(y, x, temp, r);
            //gmp_printf("Root = %Zd\n", y);
        }
    else {
        printf("quadratic non residue\n");
    }

    printf("\nYou can verify the result on SageMath (sagecell.sagemath.org) with the following code:\n");
    gmp_printf("R = Integers(%Zd)\nx = R(%Zd)\nx^((%Zd-1)/2) == 1 #True if quadratic residue\n", r, x, r);

    if(0) {
        unsigned long int counter = 1;
        while (counter < MOD + 1){
            mpz_set_ui(x, counter);
            ec_simple_equation_calculation(y, x, temp, r);
            if (is_quadratic_residue(y, temp, r) == 1) {
                gmp_printf("EC(%lu) = %Zd\t", counter, y);
                printf("quadratic res OK\t");
                //tonelli_shanks_algorithm(y, y, temp, r);
                gmp_printf("Root = %Zd\n", y);
            }
        counter ++;
        }
    }

    mpz_clear(y);
    mpz_clear(x);
    mpz_clear(r);
    mpz_tab_free(temp, TEMP_LEN);

    return 0;
}