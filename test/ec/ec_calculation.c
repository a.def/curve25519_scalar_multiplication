#include "../include/eclib.h"

int main()
{
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\nElliptic Curve Calculation x^3 + %d*x + %d modulo %d\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n", A, B, MOD);
    printf("This program calculates y^2 only. It tests the function ec_equation_calculation from eclib.h\n\n");
    mpz_t x;
    mpz_t y;
    mpz_t r;

    mpz_init(x);
    mpz_init(y);
    mpz_init_set_ui(r, MOD);

    mpz_t* temp = mpz_tab_creation(TEMP_LEN);

    unsigned long x_input;
    printf("Enter an x coord: ");
    scanf("%lu", &x_input);

    mpz_set_ui(x, x_input);
    ec_simple_equation_calculation(y, x, temp, r);
    gmp_printf("EC(%lu) = %Zd\n\n", x_input, y);

    printf("You can verify the result on SageMath (sagecell.sagemath.org) with the following code:\n");
    gmp_printf("R = Integers(%Zd)\nx = R(%Zd)\n%Zd == x^3 + %d*x + %d\n", r, x, y, A, B);

    mpz_tab_free(temp, TEMP_LEN);
    mpz_clear(x);
    mpz_clear(y);
    mpz_clear(r);


    return 0;
}

