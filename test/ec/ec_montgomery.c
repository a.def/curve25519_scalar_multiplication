#include "../include/eclib.h"


int main()
{   
    printf("~~~~~~~~~~~~~~~~~~~~~~~\nMontgomery Ladder\n~~~~~~~~~~~~~~~~~~~~~~~\n");
    printf("This function tests ec_montgomery_ladder from eclib.\n\n");
    //variable initialisation
    mpz_t r;
    mpz_t scalar;
    mpz_t* temp = mpz_tab_creation(TEMP_LEN);

    ec_point p1 = ec_point_init();
    ec_point p3 = ec_point_init();

    unsigned long scalar_input;
    printf("Enter a scalar: ");
    scanf("%lu", &scalar_input);

    mpz_init_set_ui(scalar, scalar_input);

    mpz_init_set_ui(r, MOD);

    gmp_randstate_t state;
    gmp_randinit_mt(state);
    ec_point_generator(p1, temp, state, r);

    ec_point_printf(p1, "\np1");
    //test

    ec_montgomery_ladder(p3, p1, scalar, temp, r);
    printf("%lu *", scalar_input);
    ec_point_printf(p3, "p1");

    printf("\nYou can verify the result on SageMath (sagecell.sagemath.org) with the following code:\n");
    gmp_printf("E = EllipticCurve(GF(%Zd), [%lu, %lu])\n", r, A, B);
    gmp_printf("x = E(%Zd, %Zd)\nE(%Zd, %Zd) == %lu * x\n", p1->x, p1->y, p3->x, p3->y, scalar_input);

    //We want to make sure putting the same pointer for the result and the source still yields the same result
    ec_montgomery_ladder(p1, p1, scalar, temp, r);
    assert((mpz_cmp(p1->x, p3->x)==0)&&(mpz_cmp(p1->y, p3->y)==0)&&(p1->infinity == p3->infinity));


    //liberation de la memoire alloue au programme
    ec_point_free(p1);
    ec_point_free(p3);

    mpz_tab_free(temp, TEMP_LEN);
    
    gmp_randclear(state);
    
    mpz_clear(r);
    mpz_clear(scalar);
    return 0;
}