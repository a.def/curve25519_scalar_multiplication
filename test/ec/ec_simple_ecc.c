#include "../include/eclib.h"

int main() {

    //Variable declaration
    gmp_randstate_t state;

    mpz_t prime;
    mpz_t private_key;
    mpz_t* temp  = mpz_tab_creation(TEMP_LEN);
    
    ec_point generator = ec_point_init(); 
    ec_point result = ec_point_init();

    //variable initialisation
    gmp_random_initialisation(state);
    mpz_init(prime);
    mpz_init(private_key);

    //prime calculation
    mpz_set_ui(prime, 2);
    mpz_pow_ui(prime, prime, 255);
    mpz_sub_ui(prime, prime, 19);
    size_t len = mpz_sizeinbase(prime, 2) ;

    

    //private key generation
    mpz_urandomb (private_key, state, N_BITS);
    len = mpz_sizeinbase(private_key, 2) ;
    
    

    //generating random point generator
    ec_point_generator(generator, temp, state, prime);
    ec_point_set(result, generator);
    

    //calculation of the public key
    ec_montgomery_ladder(result, result, private_key, temp, prime);
    

    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\nSimple EC Cryptographic private and public key generation\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    printf("This program will first initialise the gmp random number generator by using a random seed taken from /dev/urandom.\n");
    printf("Then it will generate a random private key and a random point on the curve y^2 = x^3 + %lld*x + %d modulo %d.\n", A, B, MOD);
    printf("Finally, the public key will be calculated by multiplying the private key with the generator.\n\n");

    gmp_printf("prime = %Zd, number of bits = %lu\n", prime, len);
    gmp_printf("private key = %Zd, number of bits = %lu\n", private_key, len);
    ec_point_printf(generator, "generator");
    ec_point_printf(result, "public key");

    printf("\nYou can verify the result on SageMath (sagecell.sagemath.org) with the following code:\n");
    gmp_printf("E = EllipticCurve(GF(%Zd), [%lu, %lu])\n", prime, A, B);
    gmp_printf("x = E(%Zd, %Zd)\nE(%Zd, %Zd) == %Zd * x\n", generator->x, generator->y, result->x, result->y, private_key);

    mpz_tab_free(temp, TEMP_LEN);
    gmp_randclear(state);
    mpz_clear(prime);
    mpz_clear(private_key);
    ec_point_free(generator);
    ec_point_free(result);

    return 1;
}