#include "../include/eclib.h"


int main()
{   
    printf("\n\n~~~~~~~~~~~~~~~~~~~~~~~\ntest addition\n\n");
    //variable initialisation
    mpz_t base;
    mpz_t r;
    mpz_t* temp = mpz_tab_creation(TEMP_LEN);

    ec_point p1 = ec_point_init_str("44148", "45897", EC_NOT_INFINITY, 10);
    ec_point p2 = ec_point_init_ui(85701, 79725, EC_NOT_INFINITY);
    ec_point p3 = ec_point_init();

    mpz_init_set_ui(base, 10);
    mpz_init_set_ui(r, MOD);
    char* str = ec_point_toString(NULL, 10, p1);
    printf("\np1 using tostring = %s\n", str);
    free(str);
    ec_point_printf(p2, "p2");

    //test
    printf("addition of p1 and p2:\n");
    ec_point_addition(p3, p1, p2, temp,  r);

    ec_point_printf(p3, "p1+p2");

    ec_point_doubling(p3, p1, temp,  r);
    ec_point_printf(p3, "p1 * 2");

    printf("\nYou can verify the result on SageMath (sagecell.sagemath.org) with the following code:\n");
    gmp_printf("E = EllipticCurve(GF(%Zd), [%lu, %lu])\n", r, A, B);
    gmp_printf("p1 = E(%Zd, %Zd)\n", p1 -> x, p1 -> y);
    gmp_printf("p2 = E(%Zd, %Zd)\n", p2 -> x, p2 -> y);
    printf("(p1 + p2, p1*2)\n");

    //liberation de la memoire alloue au programme
    mpz_tab_free(temp, TEMP_LEN);
    ec_point_free(p1);
    ec_point_free(p2);
    ec_point_free(p3);
    mpz_clear(base);
    mpz_clear(r);
    return 0;
}