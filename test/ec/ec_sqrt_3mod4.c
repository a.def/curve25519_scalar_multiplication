#include "../include/eclib.h"
#include "../include/gmp_extension.h"
#include <gmp.h>
#include <stdio.h>

int main() {
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~\ntest mpz_sqrt_3mod4 function\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    printf("This function efficiently calculates the sqrt(x) when prime = 3 mod 4.\n");
    printf("For example, prime = 30803 verifies this condition, while 98689 does not.\n\n");

    mpz_t x;
    mpz_t y;
    mpz_t r;
    mpz_t* temp = mpz_tab_creation(TEMP_LEN);
    mpz_init(x);
    mpz_init(y);
    mpz_init(r);

    unsigned long prime;
    printf("Enter a valid prime number: ");
    scanf("%lu", &prime);

    unsigned long x_input;
    printf("Enter an x coord: ");
    scanf("%lu", &x_input);

    mpz_set_ui(x, x_input);
    mpz_set_ui(r, prime);

    if (is_quadratic_residue(x, temp, r) == 1) 
    {   
        printf("x is a quadratic residue.\n");
        mpz_mod_ui(temp[0], r, 4);
        if (_mpz_cmp_ui(temp[0], 3) == 0)  {
            gmp_printf("%Zd mod 4 == 3 ; Condition met, mpz_sqrt_3mod4 can be used\n", r);
            mpz_sqrt_3mod4(y, x, temp, r);
            gmp_printf("Root = %Zd\n", y);

            printf("\nYou can verify the result on SageMath (sagecell.sagemath.org) with the following code:\n");
            gmp_printf("p = %Zd; assert is_prime(p), 'p is NOT prime'\nR = Integers(p)\nx = R(%Zd)\n", r, x);
            printf("assert (x^((p-1)/2) == 1), 'x is a quadratic non-residue'\n");
            printf("assert (p %% 4 == 3), 'Cannot use the fast squareroot algorithm: condition not verified'\n");
            gmp_printf("print('Is %Zd a root of x?', %Zd in x.sqrt(extend=False, all=True))\n", y, y);
            printf("print('Roots are', x.sqrt(extend=False, all=True))\n");
        }
        
        else {
            printf("Cannot use mpz_sqrt_3mod4 : condition not met.\n");

            printf("\nYou can verify the result on SageMath (sagecell.sagemath.org) with the following code:\n");
            gmp_printf("p = %Zd; assert is_prime(p), 'p is NOT prime'\nR = Integers(p)\nx = R(%Zd)\n", r, x);
            printf("assert (x^((p-1)/2) == 1), 'x is a quadratic non-residue'\n");
            printf("(p %% 4 == 3)\n");
        }
        
    }
    else 
    {
        printf("quadratic non residue\n");
    }

    

    mpz_clear(y);
    mpz_clear(x);
    mpz_clear(r);
    mpz_tab_free(temp, TEMP_LEN);

    return 0;
}