#include <gmp.h>
#include <stdio.h>
#include "../include/ec_point.h"

#define BASE 10

ec_point alloctest(ec_point p1, ec_point p2) {
    ec_point p3 = ec_point_init();
    ec_point_set(p3, p2);

    gmp_printf("p1 = (%Zd; %Zd; %d)\n", p1->x, p1->y,p1->infinity);
    gmp_printf("p2 = (%Zd; %Zd; %d)\n", p2->x, p2->y,p2->infinity);
    gmp_printf("p3 = (%Zd; %Zd; %d)\n", p3->x, p3->y,p3->infinity);

    return p3;
}

int main()
{   
    printf("\n\n~~~~~~~~~~~~~~~~~~~~~~~\ntest alloc_free\n\n");

    mpz_t base;
    mpz_init_set_ui(base, BASE);
    ec_point p1 = ec_point_init_str("123456879", "987654321", 0, BASE);
    ec_point p2 = ec_point_init_ui(987654321, 123456789, 0);
    ec_point p4 =ec_point_init();
    ec_point_set_str(p4, "12354", "12321", EC_NOT_INFINITY, 10);
    ec_point_printf(p4, "p4 = ");
    ec_point p3 = alloctest(p1,p2);
    ec_point_free(p1);
    ec_point_free(p2);
    ec_point_free(p3); 
    ec_point_free(p4);
    mpz_clear(base);   
    return 0;
}