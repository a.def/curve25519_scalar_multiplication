#include "../include/eclib.h"

int main()
{
    mpz_t x;
    mpz_t y;
    mpz_t base;
    mpz_t r;

    mpz_init(x);
    mpz_init(y);
    mpz_init_set_ui(base, 10);
    mpz_init_set_ui(r, MOD);

    mpz_t* temp = mpz_tab_creation(TEMP_LEN);

    unsigned long int counter = 0;
    while (counter < MOD + 1){
        mpz_set_ui(x, counter);
        ec_simple_equation_calculation(y, x, temp, r);
        gmp_printf("EC(%lu) = %Zd\n", counter, y);

        counter ++;
    }

    mpz_tab_free(temp, TEMP_LEN);
    mpz_clear(x);
    mpz_clear(y);
    mpz_clear(base);
    mpz_clear(r);


    return 0;
}

