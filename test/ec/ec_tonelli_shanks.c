#include "../include/eclib.h"
#include "../include/gmp_extension.h"
#include <gmp.h>
#include <stdio.h>

int main() {
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\ntest Tonelli-Shanks\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    printf("This function calculates the square root of the provided x value if x is indeed a quadratic residue in the field of the given prime\n");


    mpz_t x;
    mpz_t y;
    mpz_t r;
    mpz_t* temp = mpz_tab_creation(TEMP_LEN);

    unsigned long prime;
    printf("Enter a prime number: ");
    scanf("%lu", &prime);

    unsigned long x_input;
    printf("Enter an x coord: ");
    scanf("%lu", &x_input);


    mpz_init_set_ui(x, x_input);
    mpz_init(y);
    mpz_init_set_ui(r, prime);

    if (is_quadratic_residue(x, temp, r) == 1) 
    {
        printf("quadratic res\t");
        tonelli_shanks_algorithm(y, x, temp, r);
        gmp_printf("Root = %Zd\n", y);

        printf("\nYou can verify the result on SageMath (sagecell.sagemath.org) with the following code:\n");
        gmp_printf("p = %Zd; assert is_prime(p), 'p is NOT prime'\nR = Integers(p)\nx = R(%Zd)\n", r, x);
        printf("assert (x^((p-1)/2) == 1), 'x is a quadratic non-residue'\n");
        gmp_printf("print('Is %Zd a root of x?', %Zd in x.sqrt(extend=False, all=True))\n", y, y);
        printf("print('Roots are', x.sqrt(extend=False, all=True))\n");

    }
    else 
    {
        printf("quadratic non residue\n");

        printf("\nYou can verify the result on SageMath (sagecell.sagemath.org) with the following code:\n");
        gmp_printf("p = %Zd; assert is_prime(p), 'p is NOT prime'\nR = Integers(p)\nx = R(%Zd)\n", r, x);
        printf("print('Is x a quadratic residue?', (x^((p-1)/2) == 1))\n");
    }

    mpz_clear(y);
    mpz_clear(x);
    mpz_clear(r);
    mpz_tab_free(temp, TEMP_LEN);

    return 0;
}