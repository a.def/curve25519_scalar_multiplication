
#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include "../include/ec_point.h"

#include "../include/randgen.h"

int main ()
{   
    
    printf("\n\n~~~~~~~~~~~~~~~~~~~~~~~\ntest random_test\n\n");
    printf("This function tests the random generation of the seed of gmp\n");
    mpz_t test;
    unsigned long seed = randgen_urandom();
    printf("seed2 = %lu\n", seed);
    mpz_init_set_ui(test, seed); 
    gmp_printf("mpz_t test = %Zd\n", test);

    
    mpz_clear(test);
    return 0;
}