p = 0x7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffed
K = GF(p)
A = K(0x76d06)
B = K(0x01)
E = EllipticCurve(K, ((3 - A^2)/(3 * B^2), (2 * A^3 - 9 * A)/(27 * B^3)))

def to_weierstrass(A, B, x, y):
    return (x/B + A/(3*B), y/B)
    
def to_montgomery(A, B, u, v):
    return (B * (u - A/(3*B)), B*v)

G = E(*to_weierstrass(A, B, K(0x09), K(0x20ae19a1b8a086b4e01edd2c7748d14c923d4d7e6d7c61b229e9c5a27eced3d9)))

E.set_order(0x1000000000000000000000000000000014def9dea2f79cd65812631a5cf5d3ed * 0x08)

G = 2*G

print (to_montgomery(A, B, G[0], G[1]))