#include "../include/xtd_point.h"

int main()
{   
    printf("~~~~~~~~~~~~~~~\ntest alloc_free and point allocation\n~~~~~~~~~~~~~~~\n\n");

    mpz_t a;
    mpz_t b;
    mpz_t order;

    mpz_init_set_ui(a, 123);
    mpz_init_set_ui(b, 975091234);
    mpz_init_set_ui(order, 1000000);

    xtd_point p1 = xtd_point_init();
    xtd_point p2 = xtd_point_init();

    xtd_point_set_str(p2, "9876543787654421357687546542312568721", "123456789", "51235", "918237", 10);
    char* point_str = xtd_point_toString(NULL, 10, p2);
    printf("p2 using tostring: %s\n", point_str);
    
    xtd_point_set(p1, p2);
    char* point_str1 = xtd_point_toString(NULL, 10, p2);
    xtd_point_printf(p1, "xtd_point_set p1 from p2");
    printf("using tostring: %s\n\n", point_str1);

    xtd_point_set_mpz(p2, NULL, order, a, b);
    xtd_point_printf(p2, "xtd_point_set p2");
    xtd_point_mod(p2, p2, order);
    xtd_point_printf(p2, "xtd_point_mod p2");
    char* point_str2 = xtd_point_toString(NULL, 10, p2);
    printf("using tostring: %s\n\n", point_str2);

    

    xtd_point_free(p1);
    xtd_point_free(p2);
    mpz_clear(a);
    mpz_clear(b);
    mpz_clear(order);
    free(point_str);
    free(point_str1);
    free(point_str2);
    return 0;
}