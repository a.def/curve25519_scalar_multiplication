#include "../include/eclib.h"

//Modify this value
#define X_VALUE "57896044618658097711785492504343953926634992332820282019728792003956564819949"

int main() {
    printf("This program tests the Curve 25519 calculation of y with the generator x=9\n\n");
    //Initialisation des variables
    mpz_t order;
    mpz_t* temp = mpz_tab_creation(TEMP_LEN);
    proj_point p = proj_point_init();
    
    
    //Assigning values
    proj_point_set_str(p, X_VALUE, "0", "1", 10);
    mpz_init(order);
    mpz_set_ui(order, 2);
    mpz_pow_ui(order, order, 255);
    mpz_sub_ui(order, order, 19);

    //Program execution
    if (curve25519_calculation(p -> Y, p -> X, temp, order) != 1) {
        printf("Point not on the curve\n");
    }

    char* str_p = proj_point_toString(NULL, 10, p);
    printf("result: %s\n", str_p);
    free(str_p);


    //Verification on sagemath
    char* str_x = mpz_get_str(NULL, 10, p -> X);
    char* str_y = mpz_get_str(NULL, 10, p -> Y);
    printf("\nYou can verify the result on SageMath (sagecell.sagemath.org) with the following code:\n");
    printf("EC = EllipticCurve(GF(2^255-19),[0,486662,0,1,0])\n");
    printf("y_solutions = EC.lift_x(%s, all)\nprint(y_solutions)\n", str_x);
    printf("result = EC(%s, %s)\n", str_x, str_y);
    printf("print(result in y_solutions)\n");

    //freeing memory
    free(str_x);
    free(str_y);
    mpz_clear(order);
    mpz_tab_free(temp, TEMP_LEN);
    proj_point_free(p);

    return 0;
 }