from sage.all_cmdline import *

scalar = Integer(input("Enter a scalar: "))
EC = EllipticCurve(GF(2 ** 255 - 19),[0, 486662, 0, 1, 0])
G = EC(Integer(9) , Integer(14781619447589544791020593568409986887264606134616475288964881837755586237401))
G *= scalar

print(G)
