#include "../include/eclib.h"

int main() {
    printf("This program tests differential addition for x=9\n\n");
    //Initialisation des variables
    mpz_t order;
    mpz_t* temp = mpz_tab_creation(TEMP_LEN);
    proj_point p = proj_point_init();
    proj_point q = proj_point_init();
    proj_point identity = proj_point_init();
    
    
    //Assigning values
    proj_point_set_ui(p, 9, 0, 1);
    proj_point_set_ui(q, 10, 0, 1);
    proj_point_set_str(identity, "0", "1", "1", 10);
    mpz_init(order);
    mpz_set_ui(order, 2);
    mpz_pow_ui(order, order, 255);
    mpz_sub_ui(order, order, 19);

    //Program execution
    if (curve25519_calculation(p -> Y, p -> X, temp, order) != 1) {
        printf("Point not on the curve\n");
    }

    if (curve25519_calculation(q -> Y, q -> X, temp, order) != 1) {
        printf("Point not on the curve\n");
    }

    char* str_p = proj_point_toString_raw(NULL, 10, p);
    char* str_q = proj_point_toString_raw(NULL, 10, q);
    printf("p: %s\n", str_p);
    printf("q: %s\n", str_q);
    free(str_q);
    free(str_p);

    char* str_p_x = mpz_get_str(NULL, 10, p -> X);
    char* str_p_y = mpz_get_str(NULL, 10, p -> Y);
    char* str_q_x = mpz_get_str(NULL, 10, q -> X);
    char* str_q_y = mpz_get_str(NULL, 10, q -> Y);
    
    montgomery_differential_addition(p, p, q, identity, temp, order);
    printf("calcul finito\n");
    str_p = proj_point_toString_raw(NULL, 10, p);
    printf("result: %s\n", str_p);
    free(str_p);
    str_p = proj_point_toString_debug(10, p);
    printf("result en debug: %s\n", str_p);
    free(str_p);
    montgomery_differential_addition(q, p, identity, p, temp, order);
    str_p = proj_point_toString_debug(10, p);
    printf("result addition using identity: %s\n", str_p);
    free(str_p);


    //Verification on sagemath
    char* str_add_x = mpz_get_str(NULL, 10, p -> X);
    char* str_add_y = mpz_get_str(NULL, 10, p -> Y);
    printf("\nYou can verify the result on SageMath (sagecell.sagemath.org) with the following code:\n");
    printf("EC = EllipticCurve(GF(2^255-19),[0,486662,0,1,0])\n");
    printf("p = EC(%s, %s)\n", str_p_x, str_p_y);
    printf("q = EC(%s, %s)\n", str_q_x, str_q_y);
    printf("res = (%s, %s)\n", str_add_x, str_add_y);
    printf("print(p + q)\n");

    //freeing memory
    free(str_p_x);
    free(str_p_y);
    free(str_q_x);
    free(str_q_y);
    free(str_add_x);
    free(str_add_y);
    mpz_clear(order);
    mpz_tab_free(temp, TEMP_LEN);
    proj_point_free(p);

    return 0;
 }