#include "../include/gmp_extension.h"

//#if __ARDUINO_MODE == 0
#ifndef ARDUINO_ARCH_AVR
    void gmp_random_initialisation(gmp_randstate_t state)
    {   
        gmp_randinit_mt(state);
        gmp_randseed_ui(state, randgen_urandom());
    }

    void mpz_tab_printf(mpz_t tab[], size_t n, char tab_name[])
    {
        size_t i;
        printf("%s = {", tab_name);
        for (i = 0; i < n; i ++) {
            gmp_printf(" %Zd", tab[i]);
            if (i < n - 1) printf(", ");
        }
        printf("}\n");
    }
 

#endif

int mpz_legendre_gmp_extension(mpz_t y, mpz_t tab[], mpz_t order) {
    /*
    Legendre symbol is a function defined as (y/order) = y^{(order-1)/2}, zith order a prime number.
    It can have three results : 
        1 : y is a qudratic res mod order
        0 : y == 0 mod order
        -1 : y non quadratic res 
    */
    // return mpz_legendre(y, order);
    int legendre_symbol;
    mpz_sub_ui(tab[TEMP_LEN-1], order, 1);                           //p - 1
    mpz_fdiv_q_2exp(tab[TEMP_LEN-1], tab[TEMP_LEN-1], 1);                                
    left_binary_exp(tab[TEMP_LEN-1], y, tab[TEMP_LEN-1], tab, order);
    legendre_symbol = mpz_get_ui(tab[TEMP_LEN-1]);

    if (legendre_symbol == 1 || legendre_symbol == 0) {
        return legendre_symbol;
    }
    return -1;
}

int is_quadratic_residue(mpz_t y, mpz_t tab[], mpz_t order)
{   
    // test legendre symbol
    if (mpz_legendre_gmp_extension(y, tab, order) == 1) return 1;
    //if (mpz_legendre(y, order) == 1) return 1;
    return 0;
}


mpz_t* mpz_tab_creation(size_t n) {
    mpz_t* tab = (mpz_t*) malloc(n*sizeof(__mpz_struct));
    size_t i;
    for (i = 0; i < n; i ++) {
        mpz_init(tab[i]);       //typeof(tab[i]) is mpz_t
        //mpz_realloc2(tab[i], 256 + mp_bits_per_limb);
    }
    return tab;
}

void mpz_tab_free(mpz_t tab[], size_t n) {
    size_t i;
    for (i = 0; i < n; i ++) {
        mpz_clear(tab[i]);
    }
    free(tab);
}

size_t mpz_to_binary(char* str, mpz_t op)
{   
    size_t len = mpz_sizeinbase(op, 2) + 1;                         //len = number of bits on which the number op is stored + null terminator
    if (str == NULL) str = (char*) malloc(sizeof(char)*(len + 1));              
    mpz_get_str(str, 2, op);
    printf("string is %s\n", str);
    return len;
}   

unsigned long left_binary_exp(mpz_t result, mpz_t source, mpz_t exponent, mpz_t temp[], mpz_t order)
{

    char* str = mpz_get_str(NULL, 2, exponent);                                 //Creation of a str to store the bits
    size_t len = mpz_sizeinbase(exponent, 2) ;                                 //number of bits on which source is stored
    mpz_set_ui(temp[TEMP_LEN - 1], 1);                                          //initialisation of temp[TEMP_LEN - 1]                         
    for (size_t i = 0; i < len; i ++)                                 
    {   
        mpz_mul(temp[TEMP_LEN - 1], temp[TEMP_LEN - 1], temp[TEMP_LEN - 1]);              //raise to the power of 2
        if (str[i] - '0' == 1)                                                  //str[i] is either equal to 48 ('0') or 49 ('1'). substracting str[i] by '0' gives an integer which is either 1 or 0                                                            
        {
            mpz_mul(temp[TEMP_LEN - 1], temp[TEMP_LEN - 1], source);            //temp[TEMP_LEN - 1] = (temp[TEMP_LEN - 1] + source)
            mpz_mod(temp[TEMP_LEN - 1], temp[TEMP_LEN - 1], order);
        }
        
    }
    mpz_mod(temp[TEMP_LEN - 1], temp[TEMP_LEN - 1], order); 

    if (result != NULL)                                                         //In certain cases, we might not care about getting the value (example ; is source a quadratic residue)
    {   
        mpz_set(result, temp[TEMP_LEN - 1]);                                 //copy the mod of temp value in result
    }
    free(str);
    
    return mpz_get_ui(temp[TEMP_LEN - 1]);
}

void tonelli_shanks_algorithm(mpz_t result, mpz_t source, mpz_t temp[], mpz_t prime) {
    /*CONDITIONS:

    source is a quadratic residue

    */
    //We deal with trivial cases...
    if (mpz_cmp_ui(source, 0) == 0)
    {   
        mpz_set_ui(result, 0);
        return; //exit success
    }
    //from now on, source is coprime to prime
    if (mpz_cmp_ui(source, 1) == 0)
    {

        mpz_set_ui(result, 1);
        return; //exit success
    }

    //Factoring out prime-1 into highest power of 2 : p - 1 = Q*2^S, with Q an odd number
    //ie, right shifting p-1 until we hit the first bit equal to 1
    unsigned long int index;                                        
    unsigned long int counter;
    mpz_sub_ui(temp[0], prime, 1);                                          //Q = temp[0] = p - 1 = 0bXXXX10000 for example
    index = mpz_scan1(temp[0], 0);                                          //We are looking for the position of the 1st bit = 1, with the example above, right-shift = 4
    mpz_fdiv_q_2exp(temp[0], temp[0], index);                               //right shift temp[0] = temp[0] >> 4.

    //searching for a quadratic non residue in Fp
    mpz_set_ui(temp[1], 2);
    
    while ((mpz_legendre_gmp_extension(temp[1], temp, prime) != -1))                            //while loop will end, as half of integers in Fp are quadratic non-residues
    {
        mpz_add_ui(temp[1], temp[1], 1);                                    //z = temp[1]
    }

    
    //initialisation of the tonelli-shanks algorithm loop
    left_binary_exp(temp[2], temp[1], temp[0], temp, prime);                //c = temp[2] = z^Q
    left_binary_exp(temp[3], source, temp[0], temp, prime);                 //t = temp[3] = source^Q
    mpz_add_ui(temp[4], temp[0], 1);
    mpz_fdiv_q_ui(temp[4], temp[4], 2);                                     //temp[4] = (Q+1)/2
    left_binary_exp(temp[4], source, temp[4], temp, prime);                 //R = temp[4] = source^temp[4] ~~~~ R stands for Root
    
    
    while (mpz_cmp_ui(temp[3] /*temp[3] = t*/ , 1) != 0)                             
    {   
        mpz_set(temp[0], temp[3]);                                          //we store the current value of t = temp[3] in a temporary variable

        //Calculation of t^(2^counter) with the temporary variable temp[0]
        for(counter = 0; (counter < index) && (mpz_cmp_ui(temp[0], 1) != 0); counter ++)
        {
            mpz_mul(temp[0], temp[0], temp[0]);                             //after each new cycle, temp[0] = temp[0] * temp[0] = t^2^counter
            mpz_mod(temp[0], temp[0], prime);
            
        }
        

        mpz_set_ui(temp[1], 2);
        mpz_powm_ui(temp[1], temp[1], index - counter - 1, prime);          //calculation of 2^(index - counter - 1).
        mpz_powm(temp[1], temp[2], temp[1], prime);                         //b = temp[1] = c^(index - counter -1)
        index = counter;
        mpz_powm_ui(temp[2], temp[1], 2, prime);                            //c = temp[2] = b^2
        mpz_mul(temp[3], temp[3], temp[2]);                                 //t = temp[3] = t*b^2
        mpz_mul(temp[4], temp[4], temp[1]);                                 //R = temp[4] = R * b
        //modulo to stay in the finite field
        mpz_mod(temp[3], temp[3], prime);                                   //t = temp[e] = t modulo prime
        mpz_mod(temp[4], temp[4], prime);                                   //R = temp[4] = R modulo prime

        //Friendly reminder: t = temp[3] must be equal to 1 to exit the while loop
    }
    //a root has been found
    mpz_set(result, temp[4]);
    return; //exit success
}  

void mpz_sqrt_3mod4(mpz_t result, mpz_t source, mpz_t temp[], mpz_t prime)
{
    /*
    CONDITIONS:
        prime is odd prime
        source is a quadratic residue
        prime is  equal 3 mod 4

    It is the case, then prime + 1 is divisible by 4 and the root of source can be calculated using:
        source^{(p + 1) / 4}
    */
    mpz_add_ui(temp[0], prime, 1);
    mpz_fdiv_q_ui(temp[1], temp[0], 4);
    left_binary_exp(result, source, temp[1], temp, prime);
}