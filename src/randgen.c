#include "../include/randgen.h"

unsigned long randgen_urandom() {
    FILE *f = NULL;
    f = fopen ("/dev/urandom", "rb"); //read and binary mode

    if (f == NULL)
    {
        printf("Error opening /dev/urandom\n");
        fclose (f);
        exit(1);
    }

    unsigned long seed;
    size_t res;
    setbuf (f, NULL);

    res = fread (&seed, sizeof(seed), 1, f); //reads the first siweof(seed) bits of /dev/urandom
    fclose (f);

    if (res != 1) {
        printf("Error reading /dev/urandom\n");
        exit(1);}
    printf("seed = %lu\n", seed);
    return seed;
}

