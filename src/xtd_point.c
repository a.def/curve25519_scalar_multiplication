#include "../include/xtd_point.h"

//#if __ARDUINO_MODE == 0
#ifndef ARDUINO_ARCH_AVR
    //printf the point
    void xtd_point_printf(xtd_point point, char name[])
    {
            gmp_printf("%s = (%Zd : %Zd : %Zd : %Zd)\n", name, point->X, point->Y,point->T, point->Z);
    }
#endif

//initialises an edwards point in its Extended Twited Edwards Coordinates
xtd_point xtd_point_init()
{
    xtd_point point = (xtd_point) malloc(sizeof(_extended_coordinates_struct));
    mpz_init(point->X);
    mpz_init(point->Y);
    mpz_init(point->T);
    mpz_init(point->Z);
    return point;
}

//free the memory allocated to an xtd_point
void xtd_point_free(xtd_point target) 
{
    mpz_clear(target-> X);
    mpz_clear(target-> Y);    
    mpz_clear(target-> T);
    mpz_clear(target-> Z);
    free(target);
}

//copies the coordinates of source to target
void xtd_point_set(xtd_point target, xtd_point source)
{
    mpz_set(target-> X, source-> X);
    mpz_set(target-> Y, source-> Y);
    mpz_set(target-> T, source-> T);
    mpz_set(target-> Z, source-> Z);
}

//point setter using mpz_t values
void xtd_point_set_mpz(xtd_point target, mpz_t X, mpz_t Y, mpz_t T, mpz_t Z)
{
    if (X != NULL) mpz_set(target -> X, X);
    if (Y != NULL) mpz_set(target -> Y, Y);
    if (T != NULL) mpz_set(target -> T, T);
    if (Z != NULL) mpz_set(target -> Z, Z);
}

//Point setter using string inputs
void xtd_point_set_str(xtd_point target, const char* str_X, const char* str_Y, const char* str_T, const char* str_Z, int base) {
    if (str_X != NULL) mpz_init_set_str(target-> X, str_X, base);
    if (str_Y != NULL) mpz_init_set_str(target-> Y, str_Y, base);
    if (str_Z != NULL) mpz_init_set_str(target-> Z, str_Z, base);
    if (str_T != NULL) mpz_init_set_str(target-> T, str_T, base);
}

//point setter using unsigned long values
void xtd_point_set_ui(xtd_point target, unsigned long X, unsigned long Y, unsigned long T, unsigned long Z)
{
    mpz_set_ui(target -> X, X);
    mpz_set_ui(target -> Y, Y);
    mpz_set_ui(target -> T, T);
    mpz_set_ui(target -> Z, Z);
}

//returns source modulo order into result
void xtd_point_mod(xtd_point result, xtd_point source, mpz_t order) 
{
    mpz_mod(result -> X, source -> X, order);
    mpz_mod(result -> Y, source -> Y, order);
    mpz_mod(result -> T, source -> T, order);
    mpz_mod(result -> Z, source -> Z, order);
}

char* xtd_point_toString(char str[], int base, xtd_point point) {
        //getting the number of digits of x and y in the BASE. IT IS NOT THE LENGTH OF str_x!!
    size_t len_x = mpz_sizeinbase(point -> X, base); 
    size_t len_y = mpz_sizeinbase(point -> Y, base);
    size_t len_z = mpz_sizeinbase(point -> Z, base);
    size_t len_t = mpz_sizeinbase(point -> T, base);
    

    //create the memory block of STR if no space is allocated to it
    size_t len_point = len_x + len_y + len_z + len_t + 12; //(len_x : len_y : len_t : len_z)\0 --> 12 bcs of the additional 12 characters "() :\0"
    if (str == NULL) {
        str = (char*) malloc(len_point * sizeof(char));
    }

    //This looks quite ugly... The aim is to produce str = "(len_x : len_y : len_t : len_z)"
    size_t i;
    str[0] = '(';
    mpz_get_str(str + 1, base, point -> X); //add point->X to str starting from str[1]
    strcat(str, " : ");
    i = len_x + 3;

    mpz_get_str(str + i, base, point -> Y);
    strcat(str, " : ");
    i += len_y + 3;

    mpz_get_str(str + i, base, point -> T);
    strcat(str, " : ");
    i += len_t + 3;

    mpz_get_str(str + i, base, point -> Z);
    strcat(str, ")"); 

    return str;
}
