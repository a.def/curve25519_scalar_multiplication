#include "../include/proj_point.h"

//#if __ARDUINO_MODE == 0
#ifndef ARDUINO_ARCH_AVR
    //printf the point
    void proj_point_printf(proj_point point, char name[])
    {
            gmp_printf("%s = (%Zd : %Zd : %Zd)\n", name, point->X, point->Y, point->Z);
    }
#endif

//initialises an edwards point in its Extended Twited Edwards Coordinates
proj_point proj_point_init()
{
    proj_point point = (proj_point) malloc(sizeof(_projective_coordinates_struct));
    mpz_init(point->X);
    mpz_init(point->Y);
    mpz_init(point->Z);
    return point;
}

//free the memory allocated to an proj_point
void proj_point_free(proj_point target) 
{
    mpz_clear(target-> X);
    mpz_clear(target-> Y);    
    mpz_clear(target-> Z);
    free(target);
}

//copies the coordinates of source to target
void proj_point_set(proj_point target, proj_point source)
{
    mpz_set(target-> X, source-> X);
    mpz_set(target-> Y, source-> Y);
    mpz_set(target-> Z, source-> Z);
}

//point setter using mpz_t values
void proj_point_set_mpz(proj_point target, mpz_t X, mpz_t Y, mpz_t Z)
{
    if (X != NULL) mpz_set(target -> X, X);
    if (Y != NULL) mpz_set(target -> Y, Y);
    if (Z != NULL) mpz_set(target -> Z, Z);
}

//point setter using unsigned long values
void proj_point_set_ui(proj_point target, unsigned long X, unsigned long Y, unsigned long Z)
{
    mpz_set_ui(target -> X, X);
    mpz_set_ui(target -> Y, Y);
    mpz_set_ui(target -> Z, Z);
}

//point setter using strings
void proj_point_set_str(proj_point target, const char* str_X, const char* str_Y, const char* str_Z, int base) {

    if (str_X != NULL) mpz_init_set_str(target-> X, str_X, base);
    if (str_Y != NULL) mpz_init_set_str(target-> Y, str_Y, base);
    if (str_Z != NULL) mpz_init_set_str(target-> Z, str_Z, base);
}

//returns source modulo order into result
void proj_point_mod(proj_point result, proj_point source, mpz_t order) 
{
    mpz_mod(result -> X, source -> X, order);
    mpz_mod(result -> Y, source -> Y, order);
    mpz_mod(result -> Z, source -> Z, order);
}

//returns a 
void proj_point_normalize(proj_point result, proj_point source, mpz_t order) {
    mpz_invert(result -> Z, source -> Z, order);
    mpz_mul(result -> X, source -> X, result ->Z);
    mpz_mul(result -> Y, source -> Y, result ->Z);
    mpz_mod(result -> X, result ->X, order);
    mpz_mod(result -> Y, result ->Y, order);
    mpz_set_ui(result -> Z, 1);
}

//returns a string in the form (point->X : point->Y : point->Z)
char* proj_point_toString_raw(char str[], int base, proj_point point) {
    //getting the number of digits of x and y in the BASE. IT IS NOT THE LENGTH OF str_x!!
    size_t len_x = mpz_sizeinbase(point -> X, base); //FIXME: mpz_sizeinbase either returns the number of digits or the number of digit +1...so might have to be careful
    size_t len_y = mpz_sizeinbase(point -> Y, base);
    size_t len_z = mpz_sizeinbase(point -> Z, base);
    

    //create the memory block of STR if no space is allocated to it
    size_t len_point = len_x + len_y + len_z + 9; //(len_x: len_y: len_z)\0
    if (str == NULL) {
        str = (char*) malloc(len_point * sizeof(char));
    }

    size_t i;
    str[0] = '(';
    mpz_get_str(str + 1, base, point -> X); //add point->X to str starting from str[1]
    strcat(str, " : ");
    i = len_x + 3;

    mpz_get_str(str + i, base, point -> Y);
    strcat(str, " : ");
    i += len_y + 3;
    

    mpz_get_str(str + i, base, point -> Z);
    strcat(str, ")");

    return str;
}

char* proj_point_toString(char str[], int base, proj_point point) {
    //getting the number of digits of x and y in the BASE. IT IS NOT THE LENGTH OF str_x!!
    size_t len_x = mpz_sizeinbase(point -> X, base); //FIXME: mpz_sizeinbase either returns the number of digits or the number of digit +1...so might have to be careful
    size_t len_y = mpz_sizeinbase(point -> Y, base);
    

    //create the memory block of STR if no space is allocated to it
    size_t len_point = len_x + len_y + 5; //(len_x: len_y: len_z)\0
    if (str == NULL) {
        str = (char*) malloc(len_point * sizeof(char));
    }

    size_t i;
    str[0] = '(';
    mpz_get_str(str + 1, base, point -> X); //add point->X to str starting from str[1]
    strcat(str, ", ");
    i = len_x + 2;

    mpz_get_str(str + i, base, point -> Y);
    strcat(str, ")");

    return str;

}

//returns a string in the form (point->X : point->Y : point->Z)
char* proj_point_toString_debug(int base, proj_point point) {
    //getting the number of digits of x and y in the BASE. IT IS NOT THE LENGTH OF str_x!!
    size_t len_x = mpz_sizeinbase(point -> X, base); //FIXME: mpz_sizeinbase either returns the number of digits or the number of digit +1...so might have to be careful
    size_t len_y = mpz_sizeinbase(point -> Y, base);
    size_t len_z = mpz_sizeinbase(point -> Z, base);
    

    //create the memory block of STR if no space is allocated to it
    size_t len_point = len_x + len_y + len_z + 9; //(len_x: len_y: len_z)\0
    char* str = (char*) malloc(len_point * sizeof(char));

    size_t i;
    str[0] = '(';
    mpz_get_str(str + 1, base, point -> X); //add point->X to str starting from str[1]
    strcat(str, " : ");
    i = len_x + 3;

    mpz_get_str(str + i, base, point -> Y);
    strcat(str, " : ");
    i += len_y + 3;
    

    mpz_get_str(str + i, base, point -> Z);
    strcat(str, ")");

    return str;
}