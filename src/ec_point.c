#include "../include/ec_point.h"

//#if __ARDUINO_MODE == 0
#ifndef ARDUINO_ARCH_AVR
    void ec_point_printf(ec_point point, char name[])
    {
        gmp_printf("%s = (%Zd, %Zd, %d)\n", name, point->x, point->y,point->infinity);
    }
#endif

//basic initialization, with no value
ec_point ec_point_init()
{
    ec_point point = (ec_point) malloc(sizeof(_ec_point_struct));
    mpz_init(point->x);
    mpz_init(point->y);
    point->infinity = EC_NOT_INFINITY;
    return point;
}

//initializes and sets coordinates using unsigned long integers as input data type
ec_point ec_point_init_ui(unsigned x, unsigned y, int infinity)
{
    ec_point point = (ec_point) malloc(sizeof(_ec_point_struct));
    mpz_init_set_ui(point->x, x);
    mpz_init_set_ui(point->y, y);
    point->infinity = infinity;
    return point;
}

//initializes and sets coordinates using string as input data type
ec_point ec_point_init_str(char* str_x, char* str_y, int infinity, int base)
{
    ec_point point = (ec_point) malloc(sizeof(_ec_point_struct));
    mpz_init_set_str(point-> x, str_x, base);
    mpz_init_set_str(point-> y, str_y, base);
    point-> infinity = infinity;
    return point;
}

//free the memory allocated to an ec_point
void ec_point_free(ec_point target) 
{
    mpz_clear(target-> x);
    mpz_clear(target-> y);
    free(target);
}

//copies the coordinates of source to target
void ec_point_set(ec_point target, ec_point source)
{
    mpz_set(target-> x, source-> x);
    mpz_set(target-> y, source-> y);
    target-> infinity = source-> infinity;
}

//Sets the coordinates of the target
void ec_point_set_mpz(ec_point target, mpz_t x, mpz_t y, int infinity)
{
    if (x != NULL) {
        mpz_set(target -> x, x);
    }
    if (y != NULL) {
        mpz_set(target -> y, y);
    }
    target -> infinity = infinity;
}

void ec_point_set_str(ec_point target, char* str_x, char* str_y, int infinity, int base) {
    if (str_x != NULL) mpz_init_set_str(target-> x, str_x, base);
    if (str_y != NULL) mpz_init_set_str(target-> y, str_y, base);
    target -> infinity = infinity;
}

//Sets the coordinates of the target
void ec_point_set_ui(ec_point target, unsigned long x, unsigned long y, int infinity)
{
    mpz_set_ui(target -> x, x);
    mpz_set_ui(target -> y, y);
    target -> infinity = infinity;
}


void ec_point_mod(ec_point result, ec_point source, mpz_t order) {
    mpz_mod(result -> x, source -> x, order);
    mpz_mod(result -> y, source -> y, order);
}

char* ec_point_toString(char str[], int base,  ec_point point) {
    //getting the number of digits of x and y in the BASE. IT IS NOT THE LENGTH OF str_x!!
    size_t len_x = mpz_sizeinbase(point -> x, base);
    size_t len_y = mpz_sizeinbase(point -> y, base);
    

    //create the memory block of STR if no space is allocated to it
    size_t len_point = len_x + len_y + 8; //(len_x; len_y)\0
    if (str == NULL) {
        str = (char*) malloc(len_point * sizeof(char));
    }

    //This looks quite ugly... The aim is to produce str = "(x, y, infinity)"
    size_t i;
    str[0] = '(';
    mpz_get_str(str + 1, base, point -> x);
    i = len_x + 1;
    str[i] = ',';
    i += 1; //len_x + 2
    str[i] = ' ';
    i += 1; //len_x + 3
    mpz_get_str(str + i, base, point -> y);
    i += len_y; //len_x + len_y + 3
    str[i] = ',';
    i += 1; //len_x + len_y + 4
    str[i] = ' ';
    i += 1; //len_x + len_y + 5
    str[i] = '0' + point -> infinity;
    i += 1; //len_x + len_y + 6
    str[i] = ')';
    i += 1; //len_x + len_y + 7
    str[i] = '\0';

    return str;

}
