#include "../include/eclib.h"

#define LIMB(ptr, i) ((ptr) -> _mp_d[(i)])
#define NBR_LIMB(ptr) (((ptr) -> _mp_size) >= 0 ? ((ptr) -> _mp_size) : -((ptr) -> _mp_size))

//#if __ARDUINO_MODE == 0
#ifndef ARDUINO_ARCH_AVR
//not used if it is arduino
    int random_ec_point(ec_point target,  mpz_t temp[], gmp_randstate_t state, mpz_t order) 
    {
        mpz_urandomb (target-> x, state, N_BITS);
        mpz_mod(target -> x, target -> x, order);

        // calculates X^3 + a*X + b and stores it in target -> y
        ec_simple_equation_calculation(target -> y, target -> x, temp, order);
        //However, the actual y coordinate of the point is sqrt(target -> y), so...

        //is target -> y a quadratic residue?
        if (!is_quadratic_residue(target -> y, temp, order))
        {   
            //printf("not quadratic res...\n");
            return 0; //point NOT on the curve Quadratic non residue, therefore, target -> y does not have a square root. 
        }

        //target -> y is a quadratic residue. Let's calculate one of its roots
        //If the prime number is equal to 3 modulo 4, the root can be easily calculated
        mpz_mod_ui(temp[0], order, 4);                                      //This is used only once per random nnumber so I suppose its fine note to have order modulo 4 as a parameter of the function
        if (_mpz_cmp_ui(temp[0], 3) == 0)   
        {   
            printf("prime modulo 4 = 3\n");
            mpz_sqrt_3mod4(target -> y, target -> y, temp, order);          //we store the root of target -> y in target -> y
            return 1;                                                       //the point is on the curve
        }

        //else, tonello_shanks algorithm provides an efficient way to calculate one root.
        tonelli_shanks_algorithm(target -> y, target -> y, temp, order);    //we store the root of target->y in target->y
        return 1;                                                           // the point in on the curve
    }

    size_t ec_point_generator(ec_point target, mpz_t temp[], gmp_randstate_t state, mpz_t order) 
    {   
        size_t cycle_cnt = 1;
        while (!random_ec_point(target, temp, state, order)) {
            cycle_cnt += 1;
        }
        return cycle_cnt;
    }
#endif



void ec_simple_equation_calculation(mpz_t y, mpz_t x, mpz_t temp[], mpz_t order) 
{
    //calculates X^3 + a*X + b
    mpz_powm_ui(temp[0], x, 3, order);                  //X^3 stored in temp[0]
    if (A != 0)
    {
        mpz_mul_ui(temp[1], x, A);                  //a*X stored in temp[1]
    }
    mpz_add(temp[2], temp[0], temp[1]);             //X^3 + a*X in
    mpz_add_ui(temp[3], temp[2], B);
    mpz_mod(y, temp[3], order);
}

int ec_point_doubling(ec_point result, ec_point p, mpz_t temp[], mpz_t order)
{
    if (mpz_get_ui(p-> y) == 0)
    {
        ec_point_set(result, p);
        result-> infinity = EC_INFINITY;
        return EC_INFINITY; //if it is infinity point we return 1!!
    }

    if (p -> infinity == EC_INFINITY) 
    {
        ec_point_set(result, p);
        return EC_INFINITY;
    }

    result-> infinity = EC_NOT_INFINITY;
    //lambda calculation {3*(xp^2) + A} / (3yp)               
    mpz_powm_ui(temp[0], p-> x, 2, order);              //temp[0] = xp^2
    mpz_mul_ui(temp[1], temp[0], 3);                //temp[1] = 3*temp[0]
    mpz_add_ui(temp[2], temp[1], A);                   //temp[2] = temp[1] + A = 3*(xp^2) + A
    mpz_mul_ui(temp[0], p-> y, 2);                  //temp[0] = 2*yp
    assert(mpz_invert(temp[1], temp[0], order));        //temp[1] * temp[0] = 1 (mod order)
    mpz_mul(temp[3], temp[2], temp[1]);             //lambda = temp[3] = temp[2] * temp[1] = temp[2] / temp [0] (mod order)


    //xresult calculation: result-> x is mod order          
    mpz_powm_ui(temp[2], temp[3], 2, order);            //temp[2] = lambda^2 = temp[3]^2
    mpz_add(temp[0], p-> x, p-> x);                 //temp[0] = xp + xp
    mpz_sub(temp[1], temp[2], temp[0]);             //temp[1] = temp[3] - temp[0]
    //result->x is set later to avoid affecting resutl->y calculation
    

    //yresult calculation: result-> y is mod order    
    mpz_sub(temp[0], p-> x, temp[1]);            //temp[0] = xp - xr
    mpz_mod(result-> x, temp[1], order);                //result = temp[2] (mod order)
    mpz_mul(temp[1], temp[3], temp[0]);             //temp[1] = temp[3] * temp[0] = lambda * (xp - xr)
    mpz_sub(temp[2], temp[1], p-> y);               //yresult = lambda * (xp - xr) - yp
    mpz_mod(result-> y, temp[2], order);


    return EC_NOT_INFINITY;
}

//addition between 2 points of the elliptic curve
//temp[]: a tab of temporary mpz_t values
//order is the order of the ec.
int ec_point_addition(ec_point result, ec_point p, ec_point q, mpz_t temp[], mpz_t order)                    
{   
    //First; case of point at infinity
    if (p-> infinity == EC_INFINITY) 
    {
        ec_point_set(result, q);
        return EC_INFINITY;
    }

    if (q-> infinity == EC_INFINITY) 
    {
        ec_point_set(result, p);
        return EC_INFINITY;
    }
    
    //Second; different x which means we can draw a lign between them
    if (mpz_cmp(p-> x, q-> x) != 0) 
    {                                                   //they have different x coords : standard addition
        result-> infinity = EC_NOT_INFINITY;
        //lambda calculation                        
        mpz_sub(temp[0], q-> y, p-> y);                 //temp[0] = yq - yp
        mpz_sub(temp[1], q-> x, p-> x);                 //temp[1] = xq - xp
        mpz_mod(temp[1], temp[1], order);
        assert(mpz_invert(temp[2], temp[1], order));        //temp[2] * temp[1] = 1 (mod order)   
        mpz_mul(temp[3], temp[0], temp[2]);             //lambda = temp[3] = temp[0] * temp[2] = temp[0] / temp [1] (mod order)


       //xresult calculation: result-> x is mod order          
        mpz_powm_ui(temp[2], temp[3], 2, order);            //temp[2] = lambda^2 = temp[3]^2
        mpz_add(temp[0], q-> x, p-> x);                 //temp[0] = xp + xq
        mpz_sub(temp[1], temp[2], temp[0]);             //temp[1] = temp[3] - temp[0]
        
    

        //yresult calculation: result-> y is mod order    
        mpz_sub(temp[0], p-> x, temp[1]);               //temp[0] = xp - xr
        mpz_mod(result-> x, temp[1], order);            //result = temp[2] (mod order)
        mpz_mul(temp[1], temp[3], temp[0]);             //temp[1] = temp[3] * temp[0] = lambda * (xp - xr)
        mpz_sub(temp[2], temp[1], p-> y);               //yresult = lambda * (xp - xr) - yp
        mpz_mod(result-> y, temp[2], order);
        

        return EC_NOT_INFINITY;
    }

    //third; we know x coords are equal. bcs they are on the ec, if their y coords are different, then the points are symetric => vertical lign=> result at infinity
    if (mpz_cmp(p-> y, q-> y) != 0) 
    {
        ec_point_set(result, p);
        result-> infinity = EC_INFINITY;
        return EC_INFINITY;
    }
    
    if (mpz_cmp(p-> y, q-> y) == 0) 
    {
        return ec_point_doubling(result, p, temp, order);
    }

    printf("addition error: unknown case\n");
    assert(1);
    return -1;
}

void ec_montgomery_ladder(ec_point result, ec_point source, mpz_t scalar, mpz_t temp[], mpz_t order) {
    //initialisation of temporary points
    ec_point t2 = ec_point_init();
    ec_point_set(t2, source);
    ec_point_set_ui(result, 0, 0, EC_INFINITY); //neutral element
    
    //TODO: ATTENTION, CAR len depend de str, ce montgomery ladder est succeptible de subirune timing attack!!
    //TODO: il faut que len soit de la taille maxe de N_BITS!!!!! 
    char* str = mpz_get_str(NULL, 2, scalar);               //Creation of a str to store the bits
    size_t len = mpz_sizeinbase(scalar, 2) ;                //number of bits on which source is stored
    
    //Montgomery algorithm: EC addition followed by doubling
    for (size_t i = 0; i < len; i ++)                       //depending on the bit value, we store the result in either result or T2     
    {   

        
        if (str[i] - '0' == 0)
        {   
            ec_point_addition(t2, result, t2, temp, order);
            ec_point_doubling(result, result, temp, order);
        }
        else {
            ec_point_addition(result, result, t2, temp, order);
            ec_point_doubling(t2, t2, temp, order);
        }
    }

    ec_point_free(t2);
    free(str);
}


/*
+-+-+-+-+-+-+-+
 |E|D|W|A|R|D|S|
 +-+-+-+-+-+-+-+
*/

void edwards_unified_addition(xtd_point res, xtd_point point1, xtd_point point2, mpz_t temp[], mpz_t a, mpz_t d)
{
    //calculation of intermediate values
    mpz_mul(temp[0], point1 -> X, point2 -> X);         //A = X1*X2
    mpz_mul(temp[1], point1 -> Y, point2 -> Y);         //B = Y1*Y2
    mpz_mul(temp[3], point1 -> T, point2 -> T);         
    mpz_mul(temp[2], temp[3], d);                       //C = d*T1*T2
    mpz_mul(temp[3], point1 -> Z, point2 -> Z);         //D =Z1*Z2
    mpz_add(temp[4], point1 -> X, point1 -> Y);                 //X1+Y1
    mpz_add(temp[5], point2 -> X, point2 -> Y);                 //X2+Y2
    mpz_mul(temp[6], temp[5], temp[4]);                         //(X1+Y1) * (X2+Y2)
    mpz_sub(temp[5], temp[6], temp[0]);                         //(X1+Y1) * (X2+Y2) - A
    mpz_sub(temp[4], temp[5], temp[1]);                 //E = (X1+Y1) * (X2+Y2) - A - B     = X1*Y2 + Y1*X2
    mpz_sub(temp[5], temp[3], temp[2]);                 //F = D-C                           = Z1*Z2 - d*T1*T2
    mpz_add(temp[6], temp[3], temp[2]);                 //G = D+C                           = Z1*Z2 + d*T1*T2
    mpz_mul(temp[2], temp[0], a);
    mpz_sub(temp[3], temp[1], temp[2]);                 //H = B - a*A                       = Y1Y2 - a*X1*X2
    //output
    mpz_mul(res -> X, temp[4], temp[5]);                //X3 = E*F                          = (X1*Y2 + Y1*X2) * (Z1*Z2 - d*T1*T2)
    mpz_mul(res -> Y, temp[3], temp[5]);                //Y3 = G*H                          = (Z1*Z2 + d*T1*T2) * (Z1*Z2 + d*T1*T2)
    mpz_mul(res -> T, temp[4], temp[3]);                //T3 = E*H                          = (X1*Y2 + Y1*X2) * (Y1Y2 - a*X1*X2)
    mpz_mul(res -> Z, temp[5], temp[6]);                //Z3 = F*G                          = (Z1*Z2 - d*T1*T2) * (Z1*Z2 + d*T1*T2)
}

void edwards_scalar_multiplication(xtd_point result, xtd_point source, mpz_t scalar, mpz_t temp[], mpz_t order, mpz_t a, mpz_t d) {
    xtd_point t = xtd_point_init();
    char* str = mpz_get_str(NULL, 2, scalar);                                 //Creation of a str to store the bits
    size_t len = mpz_sizeinbase(scalar, 2) ;                                 //number of bits on which source is stored
    mpz_set_ui(temp[TEMP_LEN - 1], 1);                                          //initialisation of temp[TEMP_LEN - 1]                         
    for (size_t i = 0; i < len; i ++)                                 
    {   
        edwards_unified_addition(t, t, t, temp, a, d);                        // doubling
        if (str[i] - '0' == 1)                                                  //str[i] is either equal to 48 ('0') or 49 ('1'). substracting str[i] by '0' gives an integer which is either 1 or 0                                                            
        {
            edwards_unified_addition(t, t, source, temp, a, d);               // addition
            xtd_point_mod(t, t, order);
        }
        
    } 

    xtd_point_mod(result, t, order);

    free(str);
    xtd_point_free(t);
}

/*
+-+-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+
 |M|O|N|T|G|O|M|E|R|Y| |C|U|R|V|E|
 +-+-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+
*/
int curve25519_calculation(mpz_t y, mpz_t x, mpz_t temp[], mpz_t order) {
    // calculates y
    mpz_powm_ui(temp[0], x, 3, order);          //x^3
    mpz_powm_ui(temp[1], x, 2, order);          //x^2
    mpz_set_str(temp[2], "486662", 10);         
    mpz_mul(temp[3], temp[1], temp[2]);         //486662x^2
    mpz_add(temp[0], temp[0], temp[3]);         //x^3 + 486662x^2
    mpz_add(temp[0], temp[0], x);               //x^3 + 486662x^2 + x
    mpz_mod(y, temp[0], order);    

    //is y a quadratic residue?
    if (mpz_legendre_gmp_extension(y, temp, order) != 1)
    {   
        return 0; //point NOT on the curve Quadratic non residue, therefore, target -> y does not have a square root. 
    }

    //y is a quadratic residue. Let's calculate one of its roots
    //If the prime number is equal to 3 modulo 4, the root can be easily calculated
    mpz_mod_ui(temp[0], order, 4);                                      //This is used only once per random nnumber so I suppose its fine note to have order modulo 4 as a parameter of the function
    if (mpz_cmp_ui(temp[0], 3) == 0)   
    {   
        printf("prime modulo 4 = 3\n");
        mpz_sqrt_3mod4(y, y, temp, order);          //we store the root of target -> y in target -> y
        return 1;                                                       //the point is on the curve
    }

    //else, tonello_shanks algorithm provides an efficient way to calculate one root.
    tonelli_shanks_algorithm(y, y, temp, order);    //we store the root of target->y in target->y
    return 1; 
}

//TODO?normal addition for montgomery curves

//Fast differential addition; only uses the X and Z parameters of the proj_point struct
void montgomery_differential_addition(proj_point result, proj_point p, proj_point q, proj_point diff, mpz_t temp[], mpz_t order) {
    
    mpz_add(temp[0], p -> X, p -> Z);
    mpz_sub(temp[1], q -> X, q -> Z);
    mpz_mul(temp[1], temp[1], temp[0]);         //(Xp + Zp)(Xq - Zq)
    mpz_sub(temp[0], p -> X, p -> Z);
    mpz_add(temp[2], q -> X, q -> Z);
    mpz_mul(temp[2], temp[2], temp[0]);         //(Xp - Zp)(Xq + Zq)
    mpz_add(temp[3], temp[2], temp[1]);         //(Xp - Zp)(Xq + Zq) + (Xp + Zp)(Xq - Zq)
    mpz_powm_ui(temp[3], temp[3], 2, order);    //[...]^2
    mpz_sub(temp[2], temp[2], temp[1]);         //(Xp - Zp)(Xq + Zq) - (Xp + Zp)(Xq - Zq)
    mpz_powm_ui(temp[2], temp[2], 2, order);    //[...]^2
    mpz_mul(temp[0], temp[3], diff -> Z);
    mpz_mul(temp[1], temp[2], diff -> X);
    // if (mpz_cmp_ui(p -> Z, 0) == 0) {
    //     mpz_set(result -> X, q -> X);
    //     mpz_set(result -> Z, q -> Z);
    // }
    if (mpz_cmp_ui(q -> Z, 0) == 0) {
        mpz_set(result -> X, p -> X);
        mpz_set(result -> Z, p -> Z);
    }
    else {
        mpz_mod(result -> X, temp[0], order);
        mpz_mod(result -> Z, temp[1], order);
    }
}

//Fast differential doubling; only uses the X and Z parameters of the proj_point struct
void montgomery_differential_doubling(proj_point result, proj_point p, mpz_t temp[], mpz_t order) {
    mpz_add(temp[0], p -> X, p -> Z);
    mpz_powm_ui(temp[0], temp[0], 2, order);
    mpz_sub(temp[1], p -> X, p -> Z);
    mpz_powm_ui(temp[1], temp[1], 2, order);
    mpz_mul(temp[2], temp[0], temp[1]);

    if (mpz_cmp_ui(p -> Z, 0) == 0) {mpz_set(result -> X, p -> X);}
    else {mpz_mod(result -> X, temp[2], order);}

    mpz_sub(temp[0], temp[0], temp[1]);
    mpz_set_str(temp[2], "121666", 10);
    mpz_mul(temp[2], temp[2], temp[0]);
    mpz_add(temp[2], temp[2], temp[1]);
    mpz_mul(temp[0], temp[0], temp[2]);

    if (mpz_cmp_ui(p -> Z, 0) == 0) {mpz_set(result -> Z, p -> Z);}
    else {mpz_mod(result -> Z, temp[0], order);}
    
    
}

//Okeya-Sakurai y-coordinate recovery
void okeya_sakurai_y_recovery(proj_point result, proj_point p, proj_point q, proj_point plus, mpz_t temp[],  mpz_t order) {
    
    mpz_mul(temp[0], p -> X, q -> Z);       //xP · ZQ
    mpz_add(temp[1], q -> X, temp[0]);      //XQ + temp[0]
    mpz_sub(temp[2], q -> X, temp[0]);      //XQ − v1
    mpz_powm_ui(temp[2], temp[2], 2, order);//temp[2]^2
    mpz_mul(temp[2], temp[2], plus -> X);   //v3 · Xplus
    mpz_set_str(temp[0], "973324", 10);     //2*A
    mpz_mul(temp[0], temp[0], q -> Z);      //2A · ZQ 
    mpz_add(temp[1], temp[1], temp[0]);     //temp[1] + temp[0]          
    mpz_mul(temp[3], p -> X, q -> X);       //xP · XQ
    mpz_add(temp[3], temp[3], q -> Z);      //temp[3] + ZQ
    mpz_mul(temp[1], temp[1], temp[3]);
    mpz_mul(temp[0], temp[0], q -> Z);      //temp[0] * ZQ
    mpz_sub(temp[1], temp[1], temp[0]);
    mpz_mul(temp[1], temp[1], plus -> Z);   //temp[1] * ZPlus
    mpz_sub(result -> Y, temp[1], temp[2]);
    mpz_mul_ui(temp[0], p -> Y, 2);         //2B · yP 
    mpz_mul(temp[0], temp[0], q -> Z);
    mpz_mul(temp[0], temp[0], plus -> Z);
    mpz_mod(temp[0], temp[0], order);
    mpz_mul(temp[1], temp[0], q -> X);
    mpz_mul(temp[2], temp[0], q -> Z);
    mpz_mod(result -> X, temp[1], order);
    mpz_mod(result -> Z, temp[2], order);

}

void montgomery_differential_ladder(proj_point result, proj_point source, mpz_t scalar, mpz_t temp[], mpz_t order) {
    //initialisation of temporary points
    proj_point difference = proj_point_init();
    proj_point t = proj_point_init();
    proj_point_set(difference, source);
    proj_point_set(t, source); 
    proj_point_set_ui(result, 0, 1, 0);
    
    assert(mp_bits_per_limb * NBR_LIMB(scalar) == 256);
    
    for (int8_t i = NBR_LIMB(scalar) - 1; i >= 0; i --) /* We will iterate on the different limbs of the scalar */
    {
        for (int bitcnt = mp_bits_per_limb - 1; bitcnt >= 0; bitcnt --) 
        /* We will iterate on the each bit of a limb 
        ** Notice how we start from (mp_bits_per_limb - 1) :
        ** For the Montgomery ladder, we go from the most significant bit to the least significant bit
        ** It is the same reason why we start from the last limb !
        */
        {
            if ((LIMB(scalar, i) >> bitcnt) & 01) { /* if the i-th bit equal to 1 */
                montgomery_differential_addition(result, t, result, difference, temp, order);
                montgomery_differential_doubling(t, t, temp, order);
            }
            else {
                montgomery_differential_addition(t, t, result, difference, temp, order);
                montgomery_differential_doubling(result, result, temp, order);
            }
        }
    }
    //calculate the result using okeya sakura y coordinate recovery
    okeya_sakurai_y_recovery(result, difference, result, t, temp, order);

    proj_point_free(t);
    proj_point_free(difference);
    
}

void montgomery_differential_ladder_str(proj_point result, proj_point source, mpz_t scalar, mpz_t temp[], mpz_t order) {
    //initialisation of temporary points
    proj_point difference = proj_point_init();
    proj_point t = proj_point_init();
    proj_point_set(difference, source);
    proj_point_set(t, source); 
    proj_point_set_ui(result, 0, 1, 0); //FIXME; is it really the identity
    
    //TODO: ATTENTION, CAR len depend de str, ce montgomery ladder est succeptible de subirune timing attack!!
    //TODO: il faut que len soit de la taille maxe de N_BITS!!!!! 
    size_t len = mpz_sizeinbase(scalar, 2) ;                //number of bits on which source is stored
    char str[256] = {0};
    mpz_get_str(str + 255 - len, 2, scalar);               //Creation of a str to store the bits
    
    //Montgomery algorithm: EC addition followed by doubling
    for (size_t i = 0; i < 255; i ++)                       //depending on the bit value, we store the result in either result or T2     
    {   
        
        if (str[i] - '1' == 0)
        {   
            montgomery_differential_addition(result, t, result, difference, temp, order);
            montgomery_differential_doubling(t, t, temp, order);
        }
        else 
        {   
            montgomery_differential_addition(t, t, result, difference, temp, order);
            montgomery_differential_doubling(result, result, temp, order);
        }
    }  
    memset(str, 0, 256);
    //calculate the result using okeya sakura y coordinate recovery
    okeya_sakurai_y_recovery(result, difference, result, t, temp, order);

    proj_point_free(t);
    proj_point_free(difference);
    
}
    
    // for (mp_size_t i = 0; i < size; i ++) {

    //     printf("%lu\n", scalar -> _mp_d[i]);
    //     num = (scalar -> _mp_d[i]);

    //     printf("dans le sens correct: ");
    //     for (int bitcnt = mp_bits_per_limb - 1; bitcnt >= 0; bitcnt --) {
    //         printf("%d", (int) (num >> bitcnt) & 01);
    //     }
    //     printf("\n");


    //     printf("dans le sens inverse: ");
    //     for (num = (scalar -> _mp_d[i]); num != 0; num >>= 1) {
    //         printf("%d", (int) num & 01);
    //     }
    //     printf("\n\n");

    // }