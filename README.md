
# Elliptic Curve cryptography lib

Work In Progress






## How to compile and run your tests or programs

This project is written in C. A Makefile was created to make building, linking and compiling easier.

Prerequisites for tests: GCC, GMP lib or mini-gmp

- Place the files to be compiled directly in the ./test/ folder
- To compile, at the root of the project: "make"
- To run the program: ./bin/NAME_OF_TEST
## Documentation

ec_point : std elliptic curve point

proj_point : point using projective coordinates

xtd_point : point using extended edwards coordinates

- gmp_extension.c : provides additional functions to use mpz_t integers. These functions include the creation of arrays and mathematical functions

- eclib.c : include most cryptographic functions