#Little makefile to make compiling this project easier. It is by no means flawless...but...it works :')

#HOW DOES IT WORK?
#make init				~creates the necessary project organisation if the root is empty.
#make					~general make rule
#make clean				~remove files from obj/ and bin/
#make build				~builds all objects files from the sources
#make obj/<src_name>.o 	~builds <src_name>.c
#make tests				~builds tests from test/ only (NOT tests from test/ec or test/ed)
#make tests/<test> 		~builds <test>

#NEEDS THE FOLLOWING PROJECT ORGANISATION
#/
#	src/		~contains all src files .c
#	include/	~contains header files to the sources
#	test/		~contains tests files
#
#This makefile will create two other repositories if they do not already exist:
#	obj/		~contains compiler's build files .o
#	bin/		~contains executables and binaries of the tests

#Now and then, be sure to clean your project, in case some of the modifications were not taken into account when compiling the tests...
#Hope this will help my future self

#Compilator of choice
CC=gcc

#compilation options
CFLAGS=-Wall -Wextra -g 
GMPFLAGS=-lgmp -lm #-lpython3.10 -L/usr/lib/python3.10/config

#Directory variables
INC_DIR=include
SRC_DIR=src
BIN_DIR=bin
OBJ_DIR=obj
TEST_DIR=test

INC=-I$(INC_DIR)

#Config file which stores macros used for tests or executables. Can be left "" if there is no config file
CONFIG=$(INC_DIR)/ec_config.h

#Creating lists of the different files
SRC_FILES=$(wildcard $(SRC_DIR)/*.c)
OBJ_FILES = $(patsubst $(SRC_DIR)%.c, $(OBJ_DIR)%.o, $(SRC_FILES)) 					#replace .c by .o in all elements of the list SRC_FILES
BIN_FILES=$(patsubst $(TEST_DIR)/%.c, $(BIN_DIR)/%, $(wildcard $(TEST_DIR)/*.c))	
TEST_FILES=$(wildcard $(TEST_DIR)/*.c)												
TESTS=$(patsubst $(TEST_DIR)/%.c, %, $(TEST_FILES)) 								#Place in the list TEST all documents from $(TEST_DIR) which ends with .c after removing test/ from each string element 


#Global targets: Creates all tests executables and ecclib objects respectively
tests : $(BIN_FILES)
build : $(OBJ_FILES)

#Creates obj and bin directories if they do not exist already 
$(OBJ_DIR):
	mkdir $@
$(BIN_DIR):
	mkdir $@

#Creates object files
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(INC_DIR)/%.h $(CONFIG) | $(OBJ_DIR)
	$(CC) $(CFLAGS) $(GMPFLAGS) $(INC) $< -c -o $@

#Creates binary\executables in the bin/ repository.
$(BIN_DIR)/%:	$(TEST_DIR)/%.c $(OBJ_FILES) $(CONFIG) | $(BIN_DIR)
	gcc $(CFLAGS) $^ $(INC) $(GMPFLAGS) -o $@

init:
	mkdir src
	mkdir include
	mkdir test


.PHONY: clean init#Tells make that it should not target the file called "clean" if it exists, when calling make clean.
# Instead it should target the following command:
clean: 
	rm -rf $(BIN_DIR)/* $(OBJ_DIR)/*